package ec.telconet.prueba.paul.ramon.controller;

import ec.telconet.prueba.paul.ramon.repository.model.AdmiRol;
import ec.telconet.prueba.paul.ramon.service.AdmiRolService;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Paul
 */
@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
@RequestMapping(value = "/roles")
public class AdmiRolController {
    
    @Autowired
    private AdmiRolService admiRolService;
    
    @GetMapping(value = "all")
    public ResponseEntity<List<AdmiRol>> getAllAdmiRoles() {
        List<AdmiRol> admiRoles = admiRolService.getAllAdmiRoles();
        return new ResponseEntity<>(admiRoles, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<AdmiRol> getAdmiRolById(@PathVariable int id) {
        AdmiRol admiRol = admiRolService.getAdmiRolById(id);
        if (admiRol != null) {
            return new ResponseEntity<>(admiRol, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/add")
    public ResponseEntity<AdmiRol> createAdmiRol(@RequestBody AdmiRol admiRol) {
        AdmiRol newAdmiRol = admiRolService.saveAdmiRol(admiRol);
        return new ResponseEntity<>(newAdmiRol, HttpStatus.CREATED);
    }

    // Todo: Si se avanza hacer la validacion de cada campo a actualizar
    @PutMapping("/upd/{id}")
    public ResponseEntity<AdmiRol> updateAdmiRol(@PathVariable int id, @RequestBody AdmiRol admiRol) {
        AdmiRol searchAdmiRol = admiRolService.getAdmiRolById(id);
        if (searchAdmiRol != null) {
            admiRol.setId(id);
            AdmiRol updateAdminRol = admiRolService.saveAdmiRol(admiRol);
            return new ResponseEntity<>(updateAdminRol, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/dlt/{id}")
    public ResponseEntity<Void> deleteAdmiRol(@PathVariable int id) {
        AdmiRol searchAdminRol = admiRolService.getAdmiRolById(id);
        if (searchAdminRol != null) {
            admiRolService.deleteAdmiRol(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
