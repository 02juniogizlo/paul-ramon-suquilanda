package ec.telconet.prueba.paul.ramon.repository;

import ec.telconet.prueba.paul.ramon.repository.model.AdmiRol;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Paul
 */
public interface IAdmiRolRepository extends JpaRepository<AdmiRol, Integer>{
    
}
