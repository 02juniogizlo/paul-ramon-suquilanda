package ec.telconet.prueba.paul.ramon.controller;

import ec.telconet.prueba.paul.ramon.repository.model.InfoUsuarioRol;
import ec.telconet.prueba.paul.ramon.service.InfoUsuarioRolService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Paul
 */
@RestController
@CrossOrigin(origins="*",methods = {RequestMethod.GET, RequestMethod.POST})
@RequestMapping(value = "/info")
public class InfoUsuarioRolController {
    
    @Autowired
    private InfoUsuarioRolService infoUsuarioRolService;

    @GetMapping(value = "/all")
    public ResponseEntity<List<InfoUsuarioRol>> getAllTablaInfoUsuarioRol() {
        List<InfoUsuarioRol> tablaInfoUsuarioRols = infoUsuarioRolService.getAllInfoUsuarioRol();
        return new ResponseEntity<>(tablaInfoUsuarioRols, HttpStatus.OK);
    }
}
