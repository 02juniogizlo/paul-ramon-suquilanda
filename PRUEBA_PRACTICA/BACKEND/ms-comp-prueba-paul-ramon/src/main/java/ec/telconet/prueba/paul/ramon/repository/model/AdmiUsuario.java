package ec.telconet.prueba.paul.ramon.repository.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Paul
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "admi_usuario")
public class AdmiUsuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String usuario;
    private String password;
    private String nombres;
    private String apellidos;
    private String direccion;
    private String telefono;
    private String usuario_creacion;
    private Date fecha_creacion;

    // Para mapear objetos pesados en este caso una imagen
    @Lob
    private byte[] foto;
}
