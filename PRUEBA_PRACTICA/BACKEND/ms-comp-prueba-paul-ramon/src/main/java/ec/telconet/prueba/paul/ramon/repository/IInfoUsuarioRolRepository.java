package ec.telconet.prueba.paul.ramon.repository;

import ec.telconet.prueba.paul.ramon.repository.model.InfoUsuarioRol;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Paul
 */
public interface IInfoUsuarioRolRepository extends JpaRepository<InfoUsuarioRol, Integer>{
    
}
