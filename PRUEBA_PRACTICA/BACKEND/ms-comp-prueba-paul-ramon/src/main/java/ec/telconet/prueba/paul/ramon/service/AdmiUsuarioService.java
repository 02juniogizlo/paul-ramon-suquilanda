package ec.telconet.prueba.paul.ramon.service;

import ec.telconet.prueba.paul.ramon.repository.model.AdmiUsuario;
import ec.telconet.prueba.paul.ramon.repository.IAdmiUsuarioRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Paul
 */
@Service
public class AdmiUsuarioService {

    @Autowired
    private IAdmiUsuarioRepository admiUsuarioRepository;

    public List<AdmiUsuario> getAllAdmiUsuarios() {
        return admiUsuarioRepository.findAll();
    }

    public AdmiUsuario getAdmiUsuarioById(int id) {
        return admiUsuarioRepository.findById(id).orElse(null);
    }

    public AdmiUsuario saveAdmiUsuario(AdmiUsuario admiUsuario) {
        return admiUsuarioRepository.save(admiUsuario);
    }

    public void deleteAdmiUsuario(int id) {
        admiUsuarioRepository.deleteById(id);
    }
}
