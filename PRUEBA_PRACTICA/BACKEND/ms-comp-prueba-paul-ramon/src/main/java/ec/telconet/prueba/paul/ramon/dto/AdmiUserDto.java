package ec.telconet.prueba.paul.ramon.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author DELL
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdmiUserDto {
    private String usuario;
    private String password;
}
