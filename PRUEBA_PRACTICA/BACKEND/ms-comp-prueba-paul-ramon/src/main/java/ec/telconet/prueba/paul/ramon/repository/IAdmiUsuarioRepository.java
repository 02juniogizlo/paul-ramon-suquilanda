package ec.telconet.prueba.paul.ramon.repository;

import ec.telconet.prueba.paul.ramon.repository.model.AdmiUsuario;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Paul
 */
public interface IAdmiUsuarioRepository extends JpaRepository<AdmiUsuario, Integer>{
    
}
