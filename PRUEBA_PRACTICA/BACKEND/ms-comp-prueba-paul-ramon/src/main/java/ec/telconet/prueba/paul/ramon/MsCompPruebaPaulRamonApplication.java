package ec.telconet.prueba.paul.ramon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsCompPruebaPaulRamonApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsCompPruebaPaulRamonApplication.class, args);
	}

}
