package ec.telconet.prueba.paul.ramon.service;

import ec.telconet.prueba.paul.ramon.repository.IInfoUsuarioRolRepository;
import ec.telconet.prueba.paul.ramon.repository.model.InfoUsuarioRol;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Paul
 */
@Service
public class InfoUsuarioRolService {

    @Autowired
    private IInfoUsuarioRolRepository infoUsuarioRolRepository;

    public List<InfoUsuarioRol> getAllInfoUsuarioRol() {
        return infoUsuarioRolRepository.findAll();
    }

    public InfoUsuarioRol getInfoUsuarioRolById(int id) {
        return infoUsuarioRolRepository.findById(id).orElse(null);
    }

    public InfoUsuarioRol saveInfoUsuarioRol(InfoUsuarioRol infoUsuarioRol) {
        return infoUsuarioRolRepository.save(infoUsuarioRol);
    }

    public void deleteInfoUsuarioRol(int id) {
        infoUsuarioRolRepository.deleteById(id);
    }
}
