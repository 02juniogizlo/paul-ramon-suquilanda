package ec.telconet.prueba.paul.ramon.controller;

import ec.telconet.prueba.paul.ramon.repository.model.AdmiUsuario;
import ec.telconet.prueba.paul.ramon.service.AdmiUsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author DELL
 */
@RestController
@CrossOrigin(origins="*",methods = {RequestMethod.GET, RequestMethod.POST})
@RequestMapping(value = "/usuarios")
public class AdmiUsuarioController {
    
    @Autowired
    private AdmiUsuarioService admiUsuarioService;

    @GetMapping(value = "/all")
    public ResponseEntity<List<AdmiUsuario>> getAllAdmiUsuarios() {
        List<AdmiUsuario> admiUsuarios = admiUsuarioService.getAllAdmiUsuarios();
        return new ResponseEntity<>(admiUsuarios, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AdmiUsuario> getAdmiUsuarioById(@PathVariable int id) {
        AdmiUsuario admiUsuario = admiUsuarioService.getAdmiUsuarioById(id);
        if (admiUsuario != null) {
            return new ResponseEntity<>(admiUsuario, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/add")
    public ResponseEntity<AdmiUsuario> createAdmiUsuario(@RequestBody AdmiUsuario admiUsuario) {
        AdmiUsuario newAdmiUsuario = admiUsuarioService.saveAdmiUsuario(admiUsuario);
        return new ResponseEntity<>(newAdmiUsuario, HttpStatus.CREATED);
    }

    @PutMapping("/upd/{id}")
    public ResponseEntity<AdmiUsuario> updateAdmiUsuario(@PathVariable int id, @RequestBody AdmiUsuario admiUsuario) {
        AdmiUsuario searchAdminUsuario = admiUsuarioService.getAdmiUsuarioById(id);
        if (searchAdminUsuario != null) {
            admiUsuario.setId(id);
            AdmiUsuario updateAdminUsuario = admiUsuarioService.saveAdmiUsuario(admiUsuario);
            return new ResponseEntity<>(updateAdminUsuario, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/dlt/{id}")
    public ResponseEntity<Void> deleteAdmiUsuario(@PathVariable int id) {
        AdmiUsuario searchAdminUsuario = admiUsuarioService.getAdmiUsuarioById(id);
        if (searchAdminUsuario != null) {
            admiUsuarioService.deleteAdmiUsuario(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
