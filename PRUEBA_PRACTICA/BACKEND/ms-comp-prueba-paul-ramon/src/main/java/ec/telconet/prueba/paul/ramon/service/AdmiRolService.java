package ec.telconet.prueba.paul.ramon.service;

import ec.telconet.prueba.paul.ramon.repository.model.AdmiRol;
import ec.telconet.prueba.paul.ramon.repository.IAdmiRolRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Paul
 */
@Service
public class AdmiRolService {
    
    @Autowired
    private IAdmiRolRepository admiRolRepository;
    
    public List<AdmiRol> getAllAdmiRoles() {
        return admiRolRepository.findAll();
    }

    public AdmiRol getAdmiRolById(int id) {
        return admiRolRepository.findById(id).orElse(null);
    }

    public AdmiRol saveAdmiRol(AdmiRol admiRol) {
        return admiRolRepository.save(admiRol);
    }

    public void deleteAdmiRol(int id) {
        admiRolRepository.deleteById(id);
    }
}
