package ec.telconet.prueba.paul.ramon.repository.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Paul
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "info_usuario_rol")
public class InfoUsuarioRol {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String usuario_creacion;
    private Date fecha_creacion;
    private String usuario_modificacion;
    private Date fecha_modificacion;
    private String estado;
    
    @ManyToOne
    @JoinColumn(name = "usuario_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fk_info_usuario_rol_admin_usuario"))
    private AdmiUsuario adminUsuario;
    
    @ManyToOne
    @JoinColumn(name = "rol_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fk_info_usuario_rol_admin_rol"))
    private AdmiRol adminRol;

    

    
}
