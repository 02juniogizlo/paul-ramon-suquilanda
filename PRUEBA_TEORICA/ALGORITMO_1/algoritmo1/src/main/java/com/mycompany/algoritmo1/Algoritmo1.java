package com.mycompany.algoritmo1;

/**
 *
 * @author DELL
 */
public class Algoritmo1 {

    public static void main(String[] args) {
       
        /*int[] myArrayOne = {1, 2, 2, 5, 4, 6, 7, 8, 8};*/ //Test para ver si toma el izquierdo 
         /*imprimirNumeroRecurrencia(myArrayOne);   */
        
         // Prueba del algoritmo
        int[] myArray = {1, 2, 2, 5, 4, 6, 7, 8, 8, 8};
        imprimirNumeroRecurrencia(myArray);       
    }

    public static void imprimirNumeroRecurrencia(int[] myArray) {
        int numero_recurrencia = 0;
        int conteo_recurrencia = 1;
        int numero = myArray[0];
        // For para recorrer el Array
        for (int i = 1; i < myArray.length; i++) {
            // Sumar un conteo si el numero es igul
            if (myArray[i] == myArray[i - 1]) {
                conteo_recurrencia++;
            } else {
                if (conteo_recurrencia > numero_recurrencia) {
                    numero_recurrencia = conteo_recurrencia;
                    // numero = myArray[i];
                    numero = myArray[i - 1];
                }
                // conteo_recurrencia = 1+1;
                conteo_recurrencia = 1;
            }
        }
        // Verificar si en el ultimo conteo si el valor del conteo es mayor al ultimo conteo realizado
        if (conteo_recurrencia > numero_recurrencia) {
            numero_recurrencia = conteo_recurrencia;
            numero = myArray[myArray.length - 1];
        }
        // Imprimir la salida de las recurrencias
        System.out.println("Recurrencias: " + numero_recurrencia);
        System.out.println("Número: " + numero);
    }
}
